#! usr/bin/env python

from hashlib import sha1
from itertools import product
from sys import argv, exit
from signal import signal, SIGINT
from re import search
import getopt
from time import time

class Bruteforce:

    def __init__(self):

        self.standard_alphabet_flag = True
        self.v_flag = False
        self.start = 0

        self.PASSW = "fd4cef7a4e607f1fcc920ad6329a6df2df99a4e8"
        self.CHECK = ""

        self.low_a = "abcdefghijklmnopqrstuvwxyz"
        self.high_a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.numbers = "1234567890"
        self.specials = " "

        self.alphabet = ""

        self.number_of_checks = 0
        self.depth = 5

    def getwordR(self, hashword, CHECK, alphabet, counter, size):

        counter += 1
        if self.v_flag and counter >= 4:
            print CHECK + "     " + str(self.number_of_checks)

        if str(sha1(CHECK).hexdigest()) == self.PASSW:
            return CHECK

        if size < counter:
            return ""

        for char in alphabet:
            test = self.getwordR(hashword, CHECK + char, alphabet, counter, size)
            self.number_of_checks += 1
            if test:
                return test

    def bruteforce(self, hashword, upper = False, number = False, special = False, size = 0):
        counter = 0

        return self.getwordR(hashword, self.CHECK, self.alphabet, counter, size)

    def signal_handler(self, signal, frame):
        end = time()
        print ""
        print str(self.number_of_checks) + " tested passwords"
        print "time: %.2f seconds" % (end - self.start)
        exit(0)

    def helper(self):
        print "%-20s%s" % ("-h", "to get information about arguments")
        print "%-20s%s" % ("-p --password", "-p PASSW")
        print "%-20s%s" % ("-v", "for verbose")
        print "%-20s%s" % ("-l", "to bruteforce with low alphabet")
        print "%-20s%s" % ("-b", "to bruteforce with big alphabet")
        print "%-20s%s" % ("-n", "to bruteforce with numbers")
        print "%-20s%s" % ("-s", "to bruteforce with special characters")
        print "%-20s%s" % ("-d", "for max depth")


if __name__ == "__main__":

    bf = Bruteforce()
    signal(SIGINT, bf.signal_handler)
    
    if len(argv) == 1:
        bf.helper()
        exit(2)

    try:
        opts, args = getopt.getopt(argv[1:], "hp:d:vlbns", ["help", "password="])
    except getopt.GetoptError as err:
        print str(err)
        exit(2)

    for o, a in opts:
        if o == "-v":
            bf.v_flag = True
        elif o in ("-h", "--help"):
            bf.helper()
            exit(2)
        elif o in ("-p", "--password"):
            bf.PASSW = str(sha1(a).hexdigest())
        elif o in ("-d"):
            bf.depth = int(a)
        elif o == "-l":
            bf.alphabet += bf.low_a
        elif o == "-b":
            bf.alphabet += bf.high_a
        elif o == "-n":
            bf.alphabet += bf.numbers
        elif o == "-s":
            bf.alphabet += bf.specials
        else:
            assert False, "unhandeld option"

    if bf.alphabet == "":
        bf.alphabet = bf.low_a

    bf.start = time()
    passw = bf.bruteforce(bf.PASSW, size = bf.depth)
    end = time()
    if passw != "":
        print "Password is " + passw
        print "found after " + str(bf.number_of_checks) + " checks"
    else:
        print "Password not found"
        print bf.number_of_checks
    print "time: %.2f seconds" % (end - start)