#! usr/bin/env python

import getopt

from hashlib import sha1
from itertools import product
from sys import argv, exit
from signal import signal, SIGINT
from re import search

class Enigma:

    def __init__(self):

        self.v_flag = False
        self.encrypt_flag = True

        self.rotor_50 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.rotor_51 = "ADCBEHFGILJKMPNOQTRSUXVWZY"
        self.rotor_60 = "ACEDFHGIKJLNMOQPRTSUWVXZYB"
        self.rotor_61 = "AZXVTRPNDJHFLBYWUSQOMKIGEC"
        self.rotor_70 = "AZYXWVUTSRQPONMLKJIHGFEDCB"
        self.rotor_71 = "AEBCDFJGHIKOLMNPTQRSUYVWXZ"

        self.key = ""
        self.uncrypt = ""
        self.encrypt = ""

        self.rotor_one = ""
        self.rotor_two = ""
        self.rotor_three = ""

    def set_rotors(self, rotor_string, key):

        self.rotor_one = self.set_permutation(key[0], rotor_string[0] + rotor_string[1])
        self.rotor_two = self.set_permutation(key[1], rotor_string[2] + rotor_string[3], True)
        self.rotor_three = self.set_permutation(key[2], rotor_string[4] + rotor_string[5])

        if self.v_flag:
            print self.rotor_one
            print self.rotor_two
            print self.rotor_three

    def set_permutation(self, key, rotor, r_two=False):

        perm_rotor = ""

        if rotor == "50":
            if r_two:
                self.rotor_50 = self.rotor_50[::-1]
            perm_rotor = self.rotor_50[self.rotor_50.find(key):] + self.rotor_50[:self.rotor_50.find(key)]
        elif rotor == "51":
            if r_two:
                self.rotor_51 = self.rotor_51[::-1]
            perm_rotor = self.rotor_51[self.rotor_51.find(key):] + self.rotor_51[:self.rotor_51.find(key)]
        elif rotor == "60":
            if r_two:
                self.rotor_60 = self.rotor_60[::-1]
            perm_rotor = self.rotor_60[self.rotor_60.find(key):] + self.rotor_60[:self.rotor_60.find(key)]
        elif rotor == "61": 
            if r_two:
                self.rotor_61 = self.rotor_61[::-1]
            perm_rotor = self.rotor_61[self.rotor_61.find(key):] + self.rotor_61[:self.rotor_61.find(key)]
        elif rotor == "70":
            if r_two:
                self.rotor_70 = self.rotor_70[::-1]
            perm_rotor = self.rotor_70[self.rotor_70.find(key):] + self.rotor_70[:self.rotor_70.find(key)]
        elif rotor == "71": 
            if r_two:
                self.rotor_71 = self.rotor_71[::-1]
            perm_rotor = self.rotor_71[self.rotor_71.find(key):] + self.rotor_71[:self.rotor_71.find(key)]
        else:
            print "unknown rotor"
            exit(2)

        return perm_rotor

    def enigma(self):
        r = 3
        if self.encrypt_flag:
            for c in self.uncrypt:
                if r == 3:
                    self.encrypt += self.rotor_three[self.rotor_one.find(c)]
                    r = 2
                elif r == 2:
                    self.encrypt += self.rotor_three[self.rotor_two.find(c)]
                    r = 3
        else:
            for c in self.uncrypt:
                if r == 3:
                    self.encrypt += self.rotor_one[self.rotor_three.find(c)]
                    r = 2
                elif r == 2:
                    self.encrypt += self.rotor_two[self.rotor_three.find(c)]
                    r = 3   
        return self.encrypt



    def helper(self):
        print "%-20s%s" % ("-h", "to get information about arguments")
        print "%-20s%s" % ("-p --password", "-p MYPASS")
        print "%-20s%s" % ("-r --rotors", "'-r 506070' sets the rotors")
        print "%-20s%s" % ("-k --key", "'-k XXX' sets the enigma key")
        print "%-20s%s" % ("-v", "with verbose")
        print "%-20s%s" % ("-d", "to decrypt")


if __name__ == '__main__':
    
    en = Enigma()
    rotor_string = ""
    key = ""

    try:
        opts, args = getopt.getopt(argv[1:], "hp:r:k:vd", ["help", "password=", "rotors=", "key="])
    except getopt.GetoptError as err:
        print str(err)
        exit(2)

    for o, a in opts:
        if o == "-v":
            en.v_flag = True
        elif o in ("-h", "--help"):
            en.helper()
            exit(2)
        elif o in ("-p", "--password"):
            en.uncrypt = str(a.upper())
        elif o in ("-d"):
            en.encrypt_flag = False
        elif o in ("-r", "--rotors"):
            rotor_string = str(a)
        elif o in ("-k", "--key"):
            key = str(a)
        else:
            assert False, "unhandeld option"

    if len(argv) == 1:
        en.helper()
        exit(2)

    if rotor_string == "" or key == "" or en.uncrypt == "":
        print("python enigma.py -p PASS -k KEY -r 506070")
        exit(2)

    en.set_rotors(rotor_string, key)
    print en.enigma()